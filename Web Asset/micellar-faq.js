(function($) {
	jQuery(document).ready(function(){
    	$('#drop-down1').click(function() {
    	   if ($('.drop-down-image1').css('display') === 'none')
           {
                $('.drop-down-image1').slideDown('slow');
                $('#plus-icon-1').text('X');
           }
           else
           {
                $('.drop-down-image1').slideUp('slow');
                $('#plus-icon-1').text('+');
           }
		});
        $('#drop-down2').click(function() {
           if ($('.drop-down-image2').css('display') === 'none')
           {
                $('.drop-down-image2').slideDown('slow');
                $('#plus-icon-2').text('X');
           }
           else
           {
                $('.drop-down-image2').slideUp('slow');
                $('#plus-icon-2').text('+');
           }
        });
        $('#drop-down3').click(function() {
           if ($('.drop-down-image3').css('display') === 'none')
           {
                $('.drop-down-image3').slideDown('slow');
                $('#plus-icon-3').text('X');
           }
           else
           {
                $('.drop-down-image3').slideUp('slow');
                $('#plus-icon-3').text('+');
           }
        });
	});
})(jQuery)