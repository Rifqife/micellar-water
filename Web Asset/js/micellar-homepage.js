if (!window['YT']) {var YT = {loading: 0,loaded: 0};}if (!window['YTConfig']) {var YTConfig = {'host': 'http://www.youtube.com'};}if (!YT.loading) {YT.loading = 1;(function(){var l = [];YT.ready = function(f) {if (YT.loaded) {f();} else {l.push(f);}};window.onYTReady = function() {YT.loaded = 1;for (var i = 0; i < l.length; i++) {try {l[i]();} catch (e) {}}};YT.setConfig = function(c) {for (var k in c) {if (c.hasOwnProperty(k)) {YTConfig[k] = c[k];}}};var a = document.createElement('script');a.type = 'text/javascript';a.id = 'www-widgetapi-script';a.src = 'https://s.ytimg.com/yts/jsbin/www-widgetapi-vflnzpyZ4/www-widgetapi.js';a.async = true;var b = document.getElementsByTagName('script')[0];b.parentNode.insertBefore(a, b);})();}
(function($) {
	jQuery(document).ready(function(){
    	$('.kulit-normal').click(function() {
    		if ($('.micellar-left').css('display') === 'none')
    		{
                $('.drop-down').slideUp();
    			$('.micellar-left').slideDown();
    		}
    		else
    		{
    			$('.micellar-left').slideUp();
    		}
		});
        $('.kulit-kombinasi').click(function() {
            if ($('.micellar-right').css('display') === 'none')
            {
                $('.drop-down').slideUp();
                $('.micellar-right').slideDown();
            }
            else
            {
                $('.micellar-right').slideUp();
            }
        });
        $('ul.tabs li').click(function(){
            var tab_id = $(this).attr('data-tab');

            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');

            $(this).addClass('current');
            $('.tab-content#'+tab_id).addClass('current');

            $('iframe[src*="//www.youtube.com/embed/"]').each(function(i) {
                this.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
            });
        });
	});
})(jQuery)