(function($) {
	jQuery(document).ready(function(){
    	$('.drop-down-container').click(function(){
        var id = $(this).attr('data-tab');

        if ($(id).css('display') == 'none')
        {
          $(this).find('.drop-down-sign').text('X');
          $(id).slideDown();
        }
        else
        {
          $(this).find('.drop-down-sign').text('+');
          $(id).slideUp();
        }
      });
	});
})(jQuery)