if (!window['YT']) {var YT = {loading: 0,loaded: 0};}if (!window['YTConfig']) {var YTConfig = {'host': 'http://www.youtube.com'};}if (!YT.loading) {YT.loading = 1;(function(){var l = [];YT.ready = function(f) {if (YT.loaded) {f();} else {l.push(f);}};window.onYTReady = function() {YT.loaded = 1;for (var i = 0; i < l.length; i++) {try {l[i]();} catch (e) {}}};YT.setConfig = function(c) {for (var k in c) {if (c.hasOwnProperty(k)) {YTConfig[k] = c[k];}}};var a = document.createElement('script');a.type = 'text/javascript';a.id = 'www-widgetapi-script';a.src = 'https://s.ytimg.com/yts/jsbin/www-widgetapi-vflnzpyZ4/www-widgetapi.js';a.async = true;var b = document.getElementsByTagName('script')[0];b.parentNode.insertBefore(a, b);})();}
(function($) {
	jQuery(document).ready(function(){
    	$('.kulit-normal').click(function() {
    		if ($('.micellar-water-pink').css('display') === 'none')
    		{
    			$('.micellar-water-pink').slideDown('slow');
                $('.micellar-water-blue').slideUp('slow');
    		}
    		else
    		{
    			$('.micellar-water-blue').slideUp('slow');
    			$('.micellar-water-pink').slideUp('slow');
    		}
		});
        $('.kulit-kombinasi').click(function() {
            if ($('.micellar-water-blue').css('display') === 'none')
            {
                $('.micellar-water-blue').slideDown('slow');
                $('.micellar-water-pink').slideUp('slow');
            }
            else
            {
                $('.micellar-water-pink').slideUp('slow');
                $('.micellar-water-blue').slideUp('slow');
            }
        });
        $('ul.tabs li').click(function(){
            var tab_id = $(this).attr('data-tab');

            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');

            $(this).addClass('current');
            $('.tab-content#'+tab_id).addClass('current');
        });
	});
})(jQuery)