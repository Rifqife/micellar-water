(function($) {
	jQuery(document).ready(function(){
    	$('.kulit-normal').click(function() {
    		if ($('.micellar-water-pink').css('display') === 'none')
    		{
    			$('.micellar-water-pink').toggle('medium');
    		}
    		else
    		{
    			$('.micellar-water-blue').toggle('medium');
    			$('.micellar-water-pink').toggle('medium');
    		}
		});
        $('.kulit-kombinasi').click(function() {
            if ($('.micellar-water-blue').css('display') === 'none')
            {
                $('.micellar-water-blue').toggle('medium');
            }
            else
            {
                $('.micellar-water-pink').toggle('medium');
                $('.micellar-water-blue').toggle('medium');
            }
        });
        $('ul.tabs li').click(function(){
            var tab_id = $(this).attr('data-tab');

            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');

            $(this).addClass('current');
            $('.tab-content#'+tab_id).addClass('current');
        });
	});
})(jQuery)