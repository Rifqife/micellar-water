if (!window['YT']) {var YT = {loading: 0,loaded: 0};}if (!window['YTConfig']) {var YTConfig = {'host': 'http://www.youtube.com'};}if (!YT.loading) {YT.loading = 1;(function(){var l = [];YT.ready = function(f) {if (YT.loaded) {f();} else {l.push(f);}};window.onYTReady = function() {YT.loaded = 1;for (var i = 0; i < l.length; i++) {try {l[i]();} catch (e) {}}};YT.setConfig = function(c) {for (var k in c) {if (c.hasOwnProperty(k)) {YTConfig[k] = c[k];}}};var a = document.createElement('script');a.type = 'text/javascript';a.id = 'www-widgetapi-script';a.src = 'https://s.ytimg.com/yts/jsbin/www-widgetapi-vflnzpyZ4/www-widgetapi.js';a.async = true;var b = document.getElementsByTagName('script')[0];b.parentNode.insertBefore(a, b);})();}
(function($) {
	jQuery(document).ready(function(){
    	$('.left-icon').click(function() {
    		if ($('#tab-1').css('display') === 'none' && $('#tab-2').css('display') === 'block' && $('#tab-3').css('display') === 'none')
            {
                $('#tab-2').fadeOut();
                $('#tab-1').fadeIn();
                $('#current-page').text('1');
                $('.left-icon').css('background-color', '#eaeaea');
            }
            // else if ($('#tab-2').css('display') === 'none' && $('#tab-3').css('display') === 'block' && $('#tab-1').css('display') === 'none')
            // {
            //     $('#tab-3').fadeOut();
            //     $('#tab-2').fadeIn();
            //     $('#current-page').text('2');
            //     $('.right-icon').css('background-color', '#faa0ce');
            // }
            $('iframe[src*="//www.youtube.com/embed/"]').each(function(i) {
              this.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
            });
		});
        $('.right-icon').click(function () {
            if ($('#tab-2').css('display') === 'none' && $('#tab-1').css('display') === 'block' && $('#tab-3').css('display') === 'none')
            {
                $('#tab-1').fadeOut();
                $('#tab-2').fadeIn();
                $('#current-page').text('2');
                $('.left-icon').css('background-color', '#faa0ce');
                $('.right-icon').css('background-color', '#eaeaea');
            }
            // else if ($('#tab-3').css('display') === 'none' && $('#tab-2').css('display') === 'block' && $('#tab-1').css('display') === 'none')
            // {
            //     $('#tab-2').fadeOut();
            //     $('#tab-3').fadeIn();
            //     $('#current-page').text('3');
            //     $('.right-icon').css('background-color', '#eaeaea');
            // }
        });
	});
})(jQuery)